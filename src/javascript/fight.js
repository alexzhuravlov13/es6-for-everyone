export function fight(firstFighter, secondFighter) {
  // return winner
  let player1Health = firstFighter.health;
  let player2Health = secondFighter.health;
  while (true) {
    player2Health -= getDamage(firstFighter, secondFighter);
    console.log(firstFighter.name + ":" + player1Health + " hit " + secondFighter.name + ":" + player2Health);
    if (player2Health <= 0) {
      return firstFighter;
    }
    player1Health -= getDamage(secondFighter, firstFighter);
    console.log(secondFighter.name + ":" + player2Health + " hit " + firstFighter.name + ":" + player1Health);
    if (player1Health <= 0) {
      return secondFighter;
    }
  }
}

export function getDamage(attacker, enemy) {
  // damage = hit - block
  // return damage 
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  if (damage < 0) {
    return 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = getChance();
  let hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = getChance();
  let blockPower = fighter.defense * dodgeChance;
  return blockPower;
}

export function getChance() {
  // return chance
  let chance = Math.random() + 1;
  return chance;
}

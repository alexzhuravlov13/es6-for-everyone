import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';

export  function showWinnerModal(fighter) {
  const title = 'The winner is';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

export function createWinnerDetails(fighter) {

  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body-winner' });
  const winnerElement = createElement({ tagName: 'div', className: 'winner' });
  const nameElement = createElement({ tagName: 'div', className: 'name' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image' });

  nameElement.innerText = fighter.name;
  imageElement.src = fighter.source;
  
  winnerElement.append(imageElement);

  winnerDetails.append(nameElement);
  winnerDetails.append(winnerElement);

  return winnerDetails;
}

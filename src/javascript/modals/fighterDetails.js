import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

export function createFighterDetails(fighter) {

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'div', className: 'name' });
  const charactElement = createElement({ tagName: 'div', className: 'characteristics' });
  const attackElement = createElement({ tagName: 'div', className: 'characteristics-value' });
  const defenseElement = createElement({ tagName: 'div', className: 'characteristics-value' });
  const healthElement = createElement({ tagName: 'div', className: 'characteristics-value' });
  const imagePrev = createElement({ tagName: 'div', className: 'preview' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image' });

  // show fighter name, attack, defense, health, image

  nameElement.innerText = fighter.name;

  charactElement.innerHTML = `<b>Characteristics</b>`
  attackElement.innerHTML = `Attack: ${fighter.attack}`;
  defenseElement.innerHTML = `Defense: ${fighter.defense}`;
  healthElement.innerHTML = `Health: ${fighter.health}`;
  imageElement.src = fighter.source;

  fighterDetails.append(nameElement);

  imagePrev.append(imageElement);

  fighterDetails.append(imagePrev);

  charactElement.append(attackElement);
  charactElement.append(defenseElement);
  charactElement.append(healthElement);

  fighterDetails.append(charactElement);

  return fighterDetails;
}
